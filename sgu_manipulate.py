import pyautogui
import time
class SguManipulate:
    
    def move_and_close(self, posX, posY, clicks, sleep):
        time.sleep(sleep)
        pyautogui.click(x=posX, y=posY, clicks=clicks)

    def close_window(self):
        self.set_pause(2)
        pyautogui.keyDown('alt')
        pyautogui.press('f4')
        pyautogui.keyUp('alt')

    def login_on_sgu(self):
        pyautogui.press('tab')
        pyautogui.typewrite('thiago.dias')
        pyautogui.press('tab')
        pyautogui.typewrite('Krab63@c')
        pyautogui.press(['tab', 'tab', 'enter'])
    
    def write_new_password(self):
        pyautogui.typewrite('123456789')
        pyautogui.press('tab')
        pyautogui.typewrite('123456789')
        pyautogui.press('tab')
        pyautogui.press('backspace')

    def write(self, text):
        pyautogui.typewrite(text)

    def window_name(self):
        return pyautogui.getWindows()

    def set_pause(self, tempo):
        time.sleep(tempo)

# sgu = SguManipulate()
# print(sgu.window_name())