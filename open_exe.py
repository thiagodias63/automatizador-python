import pyautogui
import time

# Abrir a pasta do Senior na Área de Trabalho
pyautogui.moveTo(135, 219)
pyautogui.doubleClick()
# Clicar no SGU
pyautogui.moveTo(319, 165)
pyautogui.doubleClick()
# Fecha a janela do Senior
time.sleep(2)
pyautogui.keyDown('alt') 
pyautogui.press('f4') 
pyautogui.keyUp('alt') 
# Espera-lo abrir e clicar em OK (Login em Gestão Empresarial)
time.sleep(10)
pyautogui.moveTo(794, 647)
pyautogui.click()
# Prrencher as informações de Login
time.sleep(10)
pyautogui.press('tab')
pyautogui.typewrite('thiago.dias')
pyautogui.press('tab')
pyautogui.typewrite('Krab63@c')
pyautogui.press(['tab', 'tab', 'enter'])

# Prrencher o nome do usuário
time.sleep(10)
pyautogui.moveTo(591, 266)
pyautogui.click()
pyautogui.typewrite('thiago.dias')
# Abre a janela do usuário
pyautogui.moveTo(591, 320)
time.sleep(3)
pyautogui.doubleClick()
# Seleciona o campo de senha
time.sleep(2)
pyautogui.moveTo(519, 440)
pyautogui.click()
# Preencher a nova senha e zera o campo de e-mail
pyautogui.typewrite('123456789')
pyautogui.press('tab')
pyautogui.typewrite('123456789')
pyautogui.press('tab')
pyautogui.press('backspace')
# Marca que usuário deve alterar senha no próximo login
pyautogui.moveTo(408, 632)
pyautogui.click()
# Clica em OK
pyautogui.moveTo(848, 314)
pyautogui.click()
# Fecha a janela do SGU
time.sleep(2)
pyautogui.keyDown('alt') 
pyautogui.press('f4') 
pyautogui.keyUp('alt')

# Meus primeiros testes
# posX, posY = pyautogui.size()
# print(pyautogui.size())
# print(pyautogui.position())
# pyautogui.moveTo(posX / 2, posY / 2, 2)
# print(pyautogui.position())

# Exemplo da Documentação
# screenWidth, screenHeight = pyautogui.size()
# pyautogui.moveTo(screenWidth / 2, screenHeight / 2)
# currentMouseX, currentMouseY = pyautogui.position()
# pyautogui.moveTo(100, 150)
# pyautogui.click()
# pyautogui.moveTo()
