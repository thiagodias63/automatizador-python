import gmail_api
import change_password
import time
import ad_manipulate

mail = gmail_api.GmailApi()
password = change_password.ChangePassword()
ad = ad_manipulate.AdManipulate()
class MainClass:
    def __init__(self):
        self.fromMail = ''
        self.toMail = ''
        self.subjectMail = ''

    def reply(self, subjectMail, text, message):
        msg = mail.create_message(
                        self.fromMail,
                        self.toMail,
                        subjectMail,
                        text)
        mail.alter_message(message['id'])
        mail.send_mail(msg)
        
    def search_name(self, word):
        textoDivido = word.split(" ")
        for texto in textoDivido:
            tamanho = len(texto)
            posicaoPonto = texto.find(".")
            posicaoArroba = texto.find("@")
            if posicaoPonto > 2 and posicaoArroba == -1:
                textoComplementar = texto[posicaoPonto:tamanho]
                if len(textoComplementar) > 1:
                    return texto
                else:
                    return None

    def findHeaders(self, message):
        for header in message['payload']['headers']:
            if header['name'] == 'From':
                self.fromMail = header['value']
            if header['name'] == 'To':
                self.toMail = header['value']
            if header['name'] == 'Subject':
                self.subjectMail = header['value']

    def change_pass(self, message):
        textoCompleto = message['snippet'].lower()
        sistema = textoCompleto
        if sistema.find('rubi') > -1 or sistema.find('vetorh') > -1:
            vetorh = True
        if  sistema.find('sapiens') > -1:
            sapiens = True
        
        usuario = self.search_name(textoCompleto)

        if usuario != None and (vetorh == True or sapiens == True):
            if sapiens == True:
                password.main('sapiens', usuario)
                sistema = 'Gestão Empresarial'
            if vetorh == True:
                password.main('vetorh', usuario)
                sistema = 'Vetorh'
            if sapiens == True and vetorh == True:
                sistema = 'Gestão Empresarial e Vetorh'
            print("Troca realizada com sucesso")
            self.reply(
                'Solicitação de nova senha Senior ',
                'A senha do usuário: '+ usuario +' foi alterada para: 123456789 no ' + sistema, message)
        else:
            print("Não encontrado usuario")
            self.reply(
            'Não conseguimos atendender sua solicitação :(',
            'Não encontramos o usuário ou sistema solicitado no pedido de troca de senha', message)

    def unlock_user_accout(self, message):
        textoCompleto = message['snippet'].lower()
        usuario = self.search_name(textoCompleto)
        
        if usuario != None:
            ad.unlock_user(usuario)
            self.reply(
                'Solicitação de Desbloquear Usuário ',
                'O usuário ' + usuario + ' foi desbloqueado', message)

    def logoff_user(self, message):
        textoCompleto = message['snippet'].lower()
        usuario = self.search_name(textoCompleto)
        
        if usuario != None:
            session = ad.check_session(usuario)
            if session == True:
                ad.logoff_user()
                self.reply(
                'Solicitação de Derrubar a sessão ',
                'O usuário ' + usuario + ' teve a sessão encerrada',
                message)

    def main(self):
        while True:
            message = mail.read_message()
            if(message != None):
                print("Encontrado um email")
                self.findHeaders(message)
                if self.subjectMail.lower() == 'troca de senha':
                    self.change_pass(message)
                elif self.subjectMail.lower() == 'derrubar sessao':
                    self.logoff_user(message)
                elif self.subjectMail.lower() == 'desbloquear usuario':
                    self.unlock_user_accout(message)
            else:
                print("Não encotrou um e-mail")
            time.sleep(10)

mainC = MainClass()
mainC.main()