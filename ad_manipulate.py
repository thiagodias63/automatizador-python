'''# Verificar se conta está ativa
net user loginname /DOMAIN | FIND /I "Account Active"
net user thiago.dias /DOMAIN | find /I "Account Acitve"
# Ativar
Net user loginname /DOMAIN /active:YES
Net user thiago.dias /DOMAIN /active:YES
Net user allan.ponciano /DOMAIN /active:YES
# Mudar a senha e ativar
net user loginname newpassword /DOMAIN /Active:Yes
net user thiago.dias "123456789" /DOMAIN /Active:Yes
net user allan.ponciano "123456789" /DOMAIN
# Adicionar usuário ao grupo
net localgroup group_name UserLoginName /add
net localgroup STC_Comercial thiago.dias /add /DOMAIN
# Desconectando usuários do servidor
qwinsta /server:192.168.252.13 thiago.dias
logoff /server:192.168.252.13 rdp-tcp#0'''

'''import os
p = os.popen('net user allan.ponciano "123456789" /DOMAIN')
print(p.read())'''

'''import subprocess
p = subprocess.Popen('net user allan.ponciano "123456789" /DOMAIN', shell=True, stdout=subprocess.PIPE)
p.stdout.close()
print(p) '''

from subprocess import Popen, PIPE
class AdManipulate:
    def __init__(self):
        self.session_id = -1

    def exec_command(self, command):
        process = Popen(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        stdout = process.communicate()[0]
        if stdout.find('xito') > -1:
            return True
        else:
            return False

    def change_password(self, user):
        command = 'net user '+ user +' "123456789" /DOMAIN'
        retorno = self.exec_command(command)
        if retorno == True:
            print('Realizado')
        else:
            print("Algo deu errado")

    def unlock_user(self, user):
        command = 'net user '+ user +' /DOMAIN /active:YES'
        retorno = self.exec_command(command)
        if retorno == True:
            print('Realizado')
        else:
            print("Algo deu errado")

    def check_session(self, user):
        command = 'qwinsta '+ user +' /server:stc-server05'
        process = Popen(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        stdout = process.communicate()[0]
        self.session_id = stdout.split()[8]
        if stdout == '':
            print('Nenhum usuário encontrado')
            return False
        else:
            print("Usuário encontrado")
            return True

    def logoff_user(self):
        command = 'logoff /server:stc-server05 '+ self.session_id
        process = Popen(command, stdout=PIPE, stderr=PIPE, universal_newlines=True)
        stdout = process.communicate()[0]
        if stdout == ' ':
            print("Sessão Derrubada")
            return True
        elif stdout.find('o encontrada'):
            print('Nenhum usuário encontrado')
            return False

    def add_user_to_group(self, group, user):
        command ='net group '+ group +' '+ user +' /add /DOMAIN '
        retorno = self.exec_command(command)
        if retorno == True:
            print('Realizado')
        else:
            print("Algo deu errado")