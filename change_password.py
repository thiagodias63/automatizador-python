import sgu_manipulate
import soap_call

class ChangePassword:

    def main(self, sistema_senior, user):
        soapClient = soap_call.SoapCall()
        user_exists = soapClient.check_user_exists(user, 'U')
        if user_exists == 'T':
            print('Usuário existe')
            self.exec_sgu(sistema_senior, user)
        else:
            print('Usuário não existe')
    
    def check_screen_ready(self, sgu, screen_name):
        actived_windows = sgu.window_name()
        while actived_windows.get(screen_name) == None:
            actived_windows = sgu.window_name()
            print("esperando a tela:" + screen_name)
            sgu.set_pause(1)

    def exec_sgu(self, sistema_senior, user):
        instanceSgu = sgu_manipulate.SguManipulate()
        # Abrir a pasta do Senior na Área de Trabalho
        # instanceSgu.move_and_close(135, 219, 2, 0)
        # Clicar no SGU
        # self.check_screen_ready(instanceSgu, "Senior")
        instanceSgu.move_and_close(50, 30, 2, 0)
        # Fecha a janela do Senior
        # instanceSgu.close_window()
        # Espera-lo abrir e clicar em OK (Login em Gestão Empresarial)
        self.check_screen_ready(instanceSgu, "Gerenciador de Usuários")
        if sistema_senior == 'vetorh':
            instanceSgu.move_and_close(358, 480, 1, 0)
            instanceSgu.move_and_close(358, 510, 1, 1)
        instanceSgu.move_and_close(660, 514, 1, 0)
        # Prrencher as informações de Login
        self.check_screen_ready(instanceSgu, "Gerenciador de Usuários")
        instanceSgu.login_on_sgu()
        # Prencher o nome do usuário
        self.check_screen_ready(instanceSgu, "Gerenciador de Usuários")
        # Verificar se essa tela vai ficar no meio do monitor ou não
        # instanceSgu.move_and_close(326, 232, 1, 10)
        instanceSgu.move_and_close(570, 260, 1, 10)
        instanceSgu.write(user)
        # Abre a janela do usuário
        instanceSgu.move_and_close(570, 310, 2, 3)
        # Seleciona o campo de senha
        instanceSgu.move_and_close(400, 310, 1, 2)
        # Preencher a nova senha e zera o campo de e-mail
        instanceSgu.write_new_password()
        # Marca que usuário deve alterar senha no próximo login
        instanceSgu.move_and_close(281, 505, 1, 0)
        # Clica em OK
        instanceSgu.move_and_close(725, 184, 1, 0)
        # Fecha a janela do SGU
        instanceSgu.close_window()
        return True
        
#changePass = ChangePassword()
#changePass.main('sapiens', 'patricia.gomes')