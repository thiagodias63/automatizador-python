# 64166715638-fdn54ia81mmpfularrp62ai26onbq5hu.apps.googleusercontent.com
from __future__ import print_function
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from email.mime.text import MIMEText
import base64

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://mail.google.com/'

class GmailApi:
    def __init__(self):
        self.service = ''
        self.login_api()

    def login_api(self):
        store = file.Storage('token.json')
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
            creds = tools.run_flow(flow, store)
        service = build('gmail', 'v1', http=creds.authorize(Http()))
        self.service = service

    def get_labels(self):
        response = self.service.users().labels().list(userId='me').execute()
        labels = response['labels']
        for label in labels:
            print('Label id: %s'%label['id'])

    def read_message(self):
        response = self.service.users().messages().list(userId='me',
                                                    q='is:unread',
                                                    labelIds=['Label_3']
                                                    ).execute()
        messagesRecord = []
        if 'messages' in response:
            messagesRecord.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = self.service.users().messages().list(userId='me', 
                                            q='is:unread',
                                            pageToken=page_token, 
                                            labelIds=['Label_3']
                                            ).execute()
            if response['resultSizeEstimate'] > 0:
                messagesRecord.extend(response['messages'])
            if not messagesRecord:
                return
            
        if messagesRecord:
            message = self.service.users().messages().get(userId='me', 
            id=messagesRecord[0]['id']).execute()
            # print('Message snippet: %s' % message['snippet'])
            return message

    def alter_message(self, msg):
        message = self.service.users().messages().modify(userId='me', id=msg,
                                            body={'removeLabelIds': ['UNREAD']}
                                            ).execute()

        label_ids = message['labelIds']
        # print('Message ID: %s - With Label IDs %s' % (msg, label_ids))
        return message

    def send_mail(self, msg):
        message = (self.service.users().messages().send(userId='me',
        body=msg)
               .execute())
        return message

    def create_message(self, sender, to, subject, message_text):
        msg = MIMEText(message_text)
        msg['to'] = to
        msg['from'] = sender
        msg['subject'] = subject
        # message['References'] = references
        # message['In-Reply-To'] = inreply
        # type(msg)
        # return {'raw': base64.urlsafe_b64encode(msg.as_string())}
        #return {'raw': msg}
        # return msg
        raw = base64.urlsafe_b64encode(msg.as_bytes())
        raw = raw.decode()
        body = {'raw': raw}
        return body


''' 
gmailInst = GmailApi()
newMensage = gmailInst.read_message()
print(newMensage['id'])
print(newMensage['snippet'])
'''